import { Controller, Get, Post, Put, Delete } from '@nestjs/common';
import { ProductService } from './product.service';
import { ConfigService } from '@nestjs/config';

@Controller('products')
export class ProductController {
  constructor(
    private productService: ProductService,
    private configService: ConfigService,
  ) {}

  @Get()
  getProducts(): string {
    const aaa = this.configService.get('DB_CONFIG');
    console.log(aaa);
    return this.productService.getProducts();
  }

  @Post('/create')
  createProducts(): string {
    return this.productService.createProducts();
  }

  @Get(':id')
  detailProduct(): string {
    return this.productService.detailProduct();
  }

  @Put(':id')
  updateProduct(): string {
    return this.productService.updateProduct();
  }

  @Delete(':id')
  deleteProduct(): string {
    return this.productService.deleteProduct();
  }
}
