import { Injectable } from '@nestjs/common';

@Injectable()
export class ProductService {
  getProducts(): string {
    return 'Get list Products 1';
  }

  createProducts(): string {
    return 'Post list Products';
  }

  detailProduct(): string {
    return 'Get product by id';
  }

  updateProduct(): string {
    return 'Update product';
  }

  deleteProduct(): string {
    return 'Delete product';
  }
}
